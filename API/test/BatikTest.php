<?php
require(__DIR__.'/../vendor/autoload.php');

class BatikTest extends PHPUnit_Framework_TestCase
{
    private $client;
    private $sample_test;

    protected function setUp()
    {
        $this->client = new GuzzleHttp\Client([
            'base_uri' => 'http://batikita.herokuapp.com/index.php/batik/'
        ]);
    }

    public function testGet_All_Batik()
    {
        $response = $this->client->request('GET',"all?page=0");
         
        $this->assertEquals(200, $response->getStatusCode());

        $response_decoded = json_decode($response->getBody(), true);
         $sample = $response_decoded['hasil'][0];
           
        $this->assertArrayHasKey('nama_batik', $sample);
        $this->assertArrayHasKey('daerah_batik', $sample);
        $this->assertArrayHasKey('makna_batik', $sample);
        $this->assertArrayHasKey('harga_rendah', $sample);
        $this->assertArrayHasKey('harga_tinggi', $sample);
        $this->assertArrayHasKey('hitung_view', $sample);
        $this->assertArrayHasKey('link_batik', $sample);

        $this->assertEquals($response_decoded['total_halaman'], 19);
        $this->assertEquals($response_decoded['total_element'], 75);
        $this->assertEquals($response_decoded['min_price'], 95000);
        $this->assertEquals($response_decoded['max_price'], 395000);

        fwrite(STDERR, print_r("All Batik are retrieved successfuly!\n\n", TRUE));
    }

    public function testGet_Popular_Batik()
    {
        $response = $this->client->request('GET',"popular");
         
        $this->assertEquals(200, $response->getStatusCode());

        $response_decoded = json_decode($response->getBody(), true);
        $sample = $response_decoded['hasil'][0];
           
        $this->assertArrayHasKey('nama_batik', $sample);
        $this->assertArrayHasKey('daerah_batik', $sample);
        $this->assertArrayHasKey('makna_batik', $sample);
        $this->assertArrayHasKey('harga_rendah', $sample);
        $this->assertArrayHasKey('harga_tinggi', $sample);
        $this->assertArrayHasKey('hitung_view', $sample);
        $this->assertArrayHasKey('link_batik', $sample);

        $this->assertEquals($response_decoded['total_halaman'], 1);
        $this->assertEquals($response_decoded['total_element'], 6);
        $this->assertArrayHasKey('min_price', $response_decoded);
        $this->assertArrayHasKey('max_price', $response_decoded);

        fwrite(STDERR, print_r("Popular Batik are retrieved successfuly!\n\n", TRUE));
    }

    public function testGet_Batik_byKeyword()
    {

        $response = $this->client->request('GET',"semen?page=0");
         
        $this->assertEquals(200, $response->getStatusCode());

        $response_decoded = json_decode($response->getBody(), true);
         $sample = $response_decoded['hasil'][0];
           
        $this->assertArrayHasKey('nama_batik', $sample);
        $this->assertArrayHasKey('daerah_batik', $sample);
        $this->assertArrayHasKey('makna_batik', $sample);
        $this->assertArrayHasKey('harga_rendah', $sample);
        $this->assertArrayHasKey('harga_tinggi', $sample);
        $this->assertArrayHasKey('hitung_view', $sample);
        $this->assertArrayHasKey('link_batik', $sample);

        $this->assertEquals($response_decoded['total_halaman'], 4);
        $this->assertEquals($response_decoded['total_element'], 14);
        $this->assertEquals($response_decoded['min_price'], 141000);
        $this->assertEquals($response_decoded['max_price'], 247000);

        fwrite(STDERR, print_r("Search Batik by Keyword is successful!\n\n", TRUE));
    }

    public function testGet_Batik_byPrice()
    {

        $response = $this->client->request('GET',"harga/semen/300000/400000?page=0");
         
        $this->assertEquals(200, $response->getStatusCode());

        $response_decoded = json_decode($response->getBody(), true);
         $sample = $response_decoded['hasil'][0];
           
        $this->assertArrayHasKey('nama_batik', $sample);
        $this->assertArrayHasKey('daerah_batik', $sample);
        $this->assertArrayHasKey('makna_batik', $sample);
        $this->assertArrayHasKey('harga_rendah', $sample);
        $this->assertArrayHasKey('harga_tinggi', $sample);
        $this->assertArrayHasKey('hitung_view', $sample);
        $this->assertArrayHasKey('link_batik', $sample);

        $this->assertEquals($response_decoded['total_halaman'], 1);
        $this->assertEquals($response_decoded['total_element'], 1);
        $this->assertEquals($response_decoded['min_price'], 325000);
        $this->assertEquals($response_decoded['max_price'], 375000);

        $this->assertLessThanOrEqual($response_decoded['min_price'], 300000);
        $this->assertGreaterThanOrEqual($response_decoded['max_price'], 400000);

        fwrite(STDERR, print_r("Search Batik by Price is successful!\n\n", TRUE));
    }
}
?>