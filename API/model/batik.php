<?php
require __DIR__.'/../config/db.php';

class Batik {
    var $db;

    function __construct(){
        $this->db = connectDB();
    }

    function updateViewCount($data){
        $nama_batik = filter_var($data['batik'],FILTER_SANITIZE_STRING);
        $match = strtolower("%" . $nama_batik . "%");
        
        $query = $this->db->prepare("
            UPDATE table_batik
            SET hitung_view = hitung_view + 1
            WHERE LOWER(nama_batik) like :name
            ");
        $query->bindParam(':name', $match, PDO::PARAM_STR);
        $query->execute(); 
    }

    function retrieveAll($vars){
        $pageExist = isset($vars['page']);
    
        $query = $this->db->prepare("
                SELECT * FROM table_batik 
                order by nama_batik asc;
            ");
        $query->execute();

        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        $data = $this->pagination($result, $pageExist, $vars);
        $price = $this->evaluatePrice($data[0]);
        $arr = ['hasil' => $data[0], 'total_halaman' => $data[1], 'total_element' => $data[2], 'min_price' => $price[0], 'max_price' => $price[1]];
        $json = json_encode($arr);
        return $json;
    }

    function retrievePopular(){
         $query = $this->db->prepare("
             SELECT * FROM table_batik 
             order by hitung_view desc 
             limit 6;
          ");
        $query->execute();

        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        $price = $this->evaluatePrice($result);
        $arr = ['hasil' => $result, 'total_halaman' => 1, 'total_element' => 6, 'min_price' => $price[0], 'max_price' => $price[1]];
        $json = json_encode($arr);
        return $json;
    }

    function searchByKeyword($vars,$keyword){
        $pageExist = isset($vars['page']);
        $match = strtolower("%" . $keyword . "%");

        $query = $this->db->prepare("
          SELECT * FROM table_batik where
          LOWER(nama_batik) like :keyword or
          LOWER(makna_batik) like :keyword or
          LOWER(daerah_batik) like :keyword
          ");
        $query->bindParam(':keyword', $match, PDO::PARAM_STR);
        $query->execute();

        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        $data = $this->pagination($result, $pageExist, $vars);
        $price = $this->evaluatePrice($data[0]);
        $arr = ['hasil' => $data[0], 'total_halaman' => $data[1], 'total_element' => $data[2], 'min_price' => $price[0], 'max_price' => $price[1]];
        $json = json_encode($arr);
        return $json;
    }

    function searchByPrice($vars,$keyword,$min,$max){
        $pageExist = isset($vars['page']);
        $match = strtolower("%" . $keyword . "%");

        $query = $this->db->prepare("
            SELECT * FROM table_batik where
            ( LOWER(nama_batik) like :keyword or
            LOWER(makna_batik) like :keyword or
            LOWER(daerah_batik) like :keyword ) and
            ((harga_rendah >= :min and harga_rendah <= :max) or 
            (harga_tinggi >= :min and harga_tinggi <= :max))
            ");
        $query->bindParam(':keyword', $match, PDO::PARAM_STR);
        $query->bindParam(':min', $min, PDO::PARAM_STR);
        $query->bindParam(':max', $max, PDO::PARAM_STR);
        $query->execute();

        $result = $query->fetchAll(PDO::FETCH_ASSOC);
        $data = $this->pagination($result, $pageExist, $vars);
        $price = $this->evaluatePrice($data[0]);
        $arr = ['hasil' => $data[0], 'total_halaman' => $data[1], 'total_element' => $data[2], 'min_price' => $price[0], 'max_price' => $price[1]];
        $json = json_encode($arr);
        return $json;
    }

    function pagination($data, $pageExist, $vars){
        $halaman = 1;
        $elemen = sizeof($data);
        if ($pageExist) {
            $data = array_chunk($data,4);
            $page = $vars['page'];
            if (intval($page) < sizeof($data)) {
                $halaman = sizeof($data);
                $data = $data[$page];
            } else {
                $data = [];
                $elemen = 0;
            }
        }
        return [$data, $halaman, $elemen];
    }

    function evaluatePrice($data){
        $min = PHP_INT_MAX;
        $max = PHP_INT_MIN;
        foreach ($data as $batik) {
            $min_price = $batik['harga_rendah'];
            $max_price = $batik['harga_tinggi'];
            if ($min_price < $min) {
                $min = $min_price;
            }
            if ($max_price > $max) {
                $max = $max_price;
            }
        }
        if($min == PHP_INT_MAX) $min=0;
        if($max == PHP_INT_MIN) $max=0;
        return [$min, $max];
    }
} 
?>