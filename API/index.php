<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require 'vendor/autoload.php';
include __DIR__.'/model/batik.php';

// Debug Tools
$configuration = [
    'settings' => [
        'displayErrorDetails' => true,
    ],
];
$container = new \Slim\Container($configuration);

$container['batik'] = function ($container) {
    return new Batik();
};

$app = new \Slim\App($container);

$app->put('/batik/update', function (Request $request, Response $response) {
    $model = $this->get('batik');
    $data = $request->getParsedBody();
    $model->updateViewCount($data);
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'text/plain')
        ->write("Yaaaay");
});

$app->get('/batik/all', function (Request $request, Response $response) {    
    $model = $this->get('batik');
    $vars = $request->getQueryParams();
    $json = $model->retrieveAll($vars);
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write($json);
});

$app->get('/batik/popular', function (Request $request, Response $response) {
    $model = $this->get('batik');
    $json = $model->retrievePopular();
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write($json);
});

$app->get('/batik/{keyword}', function (Request $request, Response $response) {
    $model = $this->get('batik');
    $keyword = $request->getAttribute('keyword');
    $vars = $request->getQueryParams();
    $json = $model->searchByKeyword($vars,$keyword);
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write($json);
});

$app->get('/batik/harga/{keyword}/{min}/{max}', function (Request $request, Response $response) {
    $model = $this->get('batik');
    $keyword = $request->getAttribute('keyword');
    $min = $request->getAttribute('min');
    $max = $request->getAttribute('max');
    $vars = $request->getQueryParams();
    $json = $model->searchByPrice($vars,$keyword,$min,$max);
    return $response->withStatus(200)
        ->withHeader('Content-Type', 'application/json')
        ->write($json);
});

$app->run();
?>