package ppld4.parikesit.batikita.presenter;


import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import ppld4.parikesit.batikita.adapter.BatikAdapter;
import ppld4.parikesit.batikita.enums.SearchSign;
import ppld4.parikesit.batikita.model.Batik;
import ppld4.parikesit.batikita.model.PageResult;
import ppld4.parikesit.batikita.view.MainActivity;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MainPresenterTest {
    @Mock
    private BatikAdapter batikAdapter;
    @Mock
    private Batik batik;

    private List<Batik> batikList;
    private PageResult pageResult;

    @Mock
    private MainActivity view;

    private MainPresenter presenter;
    private Method[] methods;

    private SearchSign mSearchSign;

    @Before
    public void setUp() throws Exception {
        batikList = new ArrayList<>();
        presenter = new MainPresenter(view);
        pageResult = Mockito.mock(PageResult.class);

        methods = presenter.getClass().getMethods();
        for (Method method : methods) {
            method.setAccessible(true);
        }
    }

    @Test
    public void test_AcceptNewList() throws Exception {
        presenter.acceptNewList(pageResult);
    }

    @Test
    public void test_SetViewContent_NoResult1() throws Exception {
        presenter.setViewContent(SearchSign.CONNECTION_FAILED);
    }
}
