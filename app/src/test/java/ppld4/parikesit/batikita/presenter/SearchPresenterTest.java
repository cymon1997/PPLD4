package ppld4.parikesit.batikita.presenter;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.lang.reflect.Field;
import java.util.AbstractCollection;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.List;

import ppld4.parikesit.batikita.adapter.BatikAdapter;
import ppld4.parikesit.batikita.enums.SearchSign;
import ppld4.parikesit.batikita.model.Batik;
import ppld4.parikesit.batikita.model.PageResult;
import ppld4.parikesit.batikita.utils.SearchScrollListener;
import ppld4.parikesit.batikita.view.SearchActivity;

import static org.mockito.Mockito.when;

/**
 * Created by Victor Ardianto on 3/22/2017.
 */

@RunWith(MockitoJUnitRunner.class)
public class SearchPresenterTest {

    @Mock
    private SearchActivity view;
    @Mock
    private BatikAdapter batikAdapter;
    @Mock
    private Batik batik;
    @Mock
    private SearchScrollListener scrollListener;

    private List<Batik> batikList;
    private PageResult pageResult;
    private SearchPresenter presenter;

    private Field field_mSearchSign;
    private Field field_pageNumber;
    private Field field_flagFilter;
    private Field field_mQuery;

    @Before
    public void setUp() throws Exception {
        batikList = new ArrayList<>();
        presenter = new SearchPresenter(view, batikAdapter, scrollListener);
        pageResult = Mockito.mock(PageResult.class);

        field_mSearchSign = presenter.getClass().getDeclaredField("mSearchSign");
        field_mSearchSign.setAccessible(true);
        field_pageNumber = presenter.getClass().getDeclaredField("mPageNumber");
        field_pageNumber.setAccessible(true);
        field_flagFilter = presenter.getClass().getDeclaredField("mFlagFilter");
        field_flagFilter.setAccessible(true);
        field_mQuery = presenter.getClass().getDeclaredField("mQuery");
        field_mQuery.setAccessible(true);

        SearchSign.valueOf("NEW_QUERY"); // Just for coverage enum
    }

    @Test
    public void testNormalQueryLength() throws Exception {
        Assert.assertEquals(false, presenter.isTooShort("lorem"));
    }

    @Test
    public void testShortQueryLength() throws Exception {
        Assert.assertEquals(true, presenter.isTooShort("a"));
    }

    @Test
    public void testQuerySubmit() throws Exception {
        Assert.assertEquals(false, presenter.onQueryTextSubmit("lorem"));
    }

    @Test
    public void testShortQuerySubmit() throws Exception {
        presenter.onQueryTextSubmit("a");
        Assert.assertEquals(false, presenter.onQueryTextSubmit("a"));
    }

    @Test
    public void test_LoadMore() throws Exception {
        presenter.loadMore();
    }

    @Test
    public void test_AcceptNewList() throws Exception {
        when(pageResult.getTotalElements()).thenReturn(1);
        field_mQuery.set(presenter,"");
        pageResult.setResults(new ArrayList<Batik>());
        presenter.acceptNewList(pageResult);
    }

    @Test
    public void test_AcceptNewList2() throws Exception {

        when(pageResult.getTotalElements()).thenReturn(1);
        field_mQuery.set(presenter,"");
        pageResult.setResults(new ArrayList<Batik>());
        field_mSearchSign.set(presenter, SearchSign.LOAD_MORE);
        field_pageNumber.set(presenter, pageResult.getTotalPages() + 1);
        field_flagFilter.set(presenter,true);
        presenter.acceptNewList(pageResult);
    }

    @Test
    public void test_AcceptNewList_NoList() throws Exception {
        when(pageResult.getTotalElements()).thenReturn(0);
        presenter.acceptNewList(pageResult);
    }

    @Test
    public void test_AcceptNewList_NoList2() throws Exception {
        when(pageResult.getTotalElements()).thenReturn(0);
        field_mSearchSign.set(presenter, SearchSign.LOAD_MORE);
        presenter.acceptNewList(pageResult);
    }

    @Test
    public void test_SetViewContent_NewQuery() throws Exception {
        presenter.setViewContent(SearchSign.NEW_QUERY);
    }

    @Test
    public void test_SetViewContent_NoResult1() throws Exception {
        field_flagFilter.set(presenter,true);
        presenter.setViewContent(SearchSign.NO_RESULT);
    }

    @Test
    public void test_SetViewContent_NoResult2() throws Exception {
        field_flagFilter.set(presenter,false);
        presenter.setViewContent(SearchSign.NO_RESULT);
    }

    @Test
    public void test_SetViewContent_ConnectionFailed() throws Exception {
        presenter.setViewContent(SearchSign.CONNECTION_FAILED);
    }

    @Test
    public void test_SetViewContent_LoadMore() throws Exception {
        presenter.setViewContent(SearchSign.LOAD_MORE);
    }

    @Test
    public void testValidPriceRange() throws Exception {
        Assert.assertEquals(false, presenter.isInvalidRange(50000,100000));
    }

    @Test
    public void testInvalidPriceRange() throws Exception {
        Assert.assertEquals(true, presenter.isInvalidRange(100000,50000));
    }

    @Test
    public void testFilterQuerySubmit() throws Exception{
        field_mQuery.set(presenter,"");
        presenter.onFilterTextSubmit("10000","1000000");
        Assert.assertEquals(false,presenter.onFilterTextSubmit("10000","1000000"));
    }

    @Test
    public void testInvalidFilterQueryRangeSubmit(){
        presenter.onFilterTextSubmit("5","1");
        Assert.assertEquals(false,presenter.onFilterTextSubmit("5","1"));
    }

    @Test
    public void testLoadAll(){
        presenter.loadAll();
    }

    @Test
    public void testRetry() throws IllegalAccessException {
        field_mQuery.set(presenter,"");
        presenter.onRetryQueryTextSubmit();
    }

    @Test
    public void testRetry2() throws IllegalAccessException {
        field_mQuery.set(presenter,"lalala");
        presenter.onRetryQueryTextSubmit();
    }
}
