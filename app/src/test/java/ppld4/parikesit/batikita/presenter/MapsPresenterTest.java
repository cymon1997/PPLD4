package ppld4.parikesit.batikita.presenter;

import android.location.Location;
import android.support.v4.content.ContextCompat;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.model.PlaceDetails;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import ppld4.parikesit.batikita.view.MapsActivity;

import static org.mockito.Mockito.when;

/**
 * Created by Victor Ardianto on 4/26/2017.
 */

@RunWith(MockitoJUnitRunner.class)
public class MapsPresenterTest {

    @Mock
    private MapsActivity view;
    private MapsPresenter presenter;

    @Before
    public void setUp() throws Exception {
        presenter = new MapsPresenter(view);
    }

    @Test
    public void test_distance() throws Exception {
        Assert.assertEquals(157249, presenter.distanceFrom(new Location(""), new LatLng(1,1)), 1);
    }

    @Test
    public void test_sortPlacesByDistance() throws Exception {
        List<PlaceDetails> listPlaces = new ArrayList<>();
        PlaceDetails a = new PlaceDetails();
        PlaceDetails b = new PlaceDetails();
        a.rating = 0;
        b.rating = 1;
        listPlaces.add(a);
        listPlaces.add(b);
        Assert.assertNotNull(presenter.sortPlacesByDistance(listPlaces));
    }

    @Test
    public void test_sortPlacesByDistance2() throws Exception {
        List<PlaceDetails> listPlaces = new ArrayList<>();
        PlaceDetails a = new PlaceDetails();
        PlaceDetails b = new PlaceDetails();
        a.rating = 1;
        b.rating = 0;
        listPlaces.add(a);
        listPlaces.add(b);
        Assert.assertNotNull(presenter.sortPlacesByDistance(listPlaces));
    }

    @Test
    public void test_sortPlacesByDistance3() throws Exception {
        List<PlaceDetails> listPlaces = new ArrayList<>();
        PlaceDetails a = new PlaceDetails();
        PlaceDetails b = new PlaceDetails();
        a.rating = 0;
        b.rating = 0;
        listPlaces.add(a);
        listPlaces.add(b);
        Assert.assertNotNull(presenter.sortPlacesByDistance(listPlaces));
    }

    @Test
    public void test_checkPermission() throws Exception {
        when(ContextCompat.checkSelfPermission(view, android.Manifest.permission.ACCESS_FINE_LOCATION)).thenReturn(1);
        presenter.checkPermission();
    }

    @Test
    public void test_checkPermission2() throws Exception {
        when(ContextCompat.checkSelfPermission(view, android.Manifest.permission.ACCESS_FINE_LOCATION)).thenReturn(0);
        presenter.checkPermission();
    }
}
