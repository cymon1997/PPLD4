package ppld4.parikesit.batikita.presenter;

import android.content.Intent;
import android.util.Log;

import ppld4.parikesit.batikita.enums.SearchSign;
import ppld4.parikesit.batikita.interfaces.BatikService;
import ppld4.parikesit.batikita.interfaces.ImageActivity;
import ppld4.parikesit.batikita.interfaces.Presenter;
import ppld4.parikesit.batikita.model.Batik;
import ppld4.parikesit.batikita.model.PageResult;
import ppld4.parikesit.batikita.utils.RetrofitService;
import ppld4.parikesit.batikita.view.GalleryActivity;
import ppld4.parikesit.batikita.view.InfoDetailActivity;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Rachel on 5/2/17.
 */

public class ImagePresenter implements Presenter {

    private final String[] QUERY_FOR_RANDOM = {"kawung"};
    private final int QUERY_FOR_RANDOM_LENGTH = QUERY_FOR_RANDOM.length;

    private Batik mBatik;
    private ImageActivity view;
    private BatikService mService;
    private RetrofitService mRetrofitService;
    private SearchSign mSearchSign = SearchSign.NEW_QUERY;

    public ImagePresenter(ImageActivity view) {
        this.view = view;

        this.mRetrofitService = new RetrofitService(this);
        this.mService = new Retrofit.Builder()
                .baseUrl("https://batikita.herokuapp.com/index.php/batik/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(BatikService.class);
    }

    public void getDummyResult() {
        mSearchSign = SearchSign.NEW_QUERY;
        String s = getRandomQuery();
        Log.d("debug",s);
        mRetrofitService.setCreateCall(mService.getRows(s, 0));
        mRetrofitService.doEnqueue();

    }

    @Override
    public void acceptNewList(PageResult result) {
        if (result.getTotalElements() == 0) {
            mSearchSign = SearchSign.NO_RESULT;
        } else {
            mBatik = result.getResults().get(0);
        }
        setViewContent(mSearchSign);
    }

    @Override
    public void setViewContent(SearchSign sign) {
        if (sign == SearchSign.NEW_QUERY) {
            view.resetView();
            view.openInfoDetail(mBatik);
        } else if (sign == SearchSign.NO_RESULT){
            view.showNoResult("Sorry, There's No Such Batik Exist");
            mSearchSign = SearchSign.NEW_QUERY;
        } else if (sign == SearchSign.CONNECTION_FAILED) {
            view.showNoResult("Failed to Connect to Server");
            mSearchSign = SearchSign.NEW_QUERY;
        }
    }

    private String getRandomQuery() {
        return QUERY_FOR_RANDOM[(int) (Math.random()* QUERY_FOR_RANDOM_LENGTH)];
    }
}
