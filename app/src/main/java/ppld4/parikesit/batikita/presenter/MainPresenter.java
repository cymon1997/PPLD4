package ppld4.parikesit.batikita.presenter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import ppld4.parikesit.batikita.R;
import ppld4.parikesit.batikita.adapter.BatikAdapter;
import ppld4.parikesit.batikita.enums.SearchSign;
import ppld4.parikesit.batikita.interfaces.BatikService;
import ppld4.parikesit.batikita.interfaces.Presenter;
import ppld4.parikesit.batikita.interfaces.RetrofitPresenter;
import ppld4.parikesit.batikita.model.Batik;
import ppld4.parikesit.batikita.model.PageResult;
import ppld4.parikesit.batikita.utils.ListHandler;
import ppld4.parikesit.batikita.utils.RetrofitService;
import ppld4.parikesit.batikita.utils.SearchScrollListener;
import ppld4.parikesit.batikita.view.InfoDetailActivity;
import ppld4.parikesit.batikita.view.MainActivity;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by George A. Pitoy on 5/2/2017.
 */

public class MainPresenter implements Presenter {
    private MainActivity view;

    private BatikService mService;
    private ListHandler mListHandler;
    private RetrofitService mRetrofitService;
    private SearchScrollListener mScrollListener;

    private Context mContext;
    private List<CardView> cardList;

    public MainPresenter(MainActivity view) {
        this.view = view;
        this.mRetrofitService = new RetrofitService(this);
        this.mService = new Retrofit.Builder()
                .baseUrl("https://batikita.herokuapp.com/index.php/batik/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(BatikService.class);

        mRetrofitService.setCreateCall(mService.getRowsPopular());
        mRetrofitService.doEnqueue();
    }

    public void acceptNewList(final PageResult result) {
        view.loadPopular(result.getResults());
    }

    public void setViewContent(SearchSign sign) {
        view.hidePopular();
    }
}
