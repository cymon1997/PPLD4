package ppld4.parikesit.batikita.presenter;

import android.util.Log;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import ppld4.parikesit.batikita.adapter.BatikAdapter;
import ppld4.parikesit.batikita.enums.SearchSign;
import ppld4.parikesit.batikita.interfaces.BatikService;

import ppld4.parikesit.batikita.interfaces.Presenter;
import ppld4.parikesit.batikita.interfaces.RetrofitPresenter;
import ppld4.parikesit.batikita.model.Batik;
import ppld4.parikesit.batikita.model.PageResult;
import ppld4.parikesit.batikita.utils.ListHandler;
import ppld4.parikesit.batikita.utils.RetrofitService;
import ppld4.parikesit.batikita.utils.SearchScrollListener;
import ppld4.parikesit.batikita.view.SearchActivity;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Victor Ardianto on 3/22/2017.
 */

public class SearchPresenter implements Presenter {

    private SearchSign mSearchSign = SearchSign.NEW_QUERY;

    private int mTotalPages;
    private int mPageNumber;
    private String mQuery;

    private BatikService mService;
    private ListHandler mListHandler;
    private RetrofitService mRetrofitService;
    private SearchScrollListener mScrollListener;
    private SearchActivity view;

    private boolean mFlagFilter;
    private int  mMinPrice;
    private int mMaxPrice;

    public SearchPresenter(SearchActivity view, BatikAdapter batikAdapter, SearchScrollListener mScrollListener) {
        this.view = view;
        this.mScrollListener = mScrollListener;
        this.mFlagFilter = false;
        this.mMinPrice = Integer.MIN_VALUE;
        this.mMaxPrice = Integer.MAX_VALUE;

        this.mListHandler = new ListHandler(batikAdapter);
        this.mRetrofitService = new RetrofitService(this);
        this.mService = new Retrofit.Builder()
                .baseUrl("https://batikita.herokuapp.com/index.php/batik/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(BatikService.class);
    }

    public void resetPresenter(){
        this.mScrollListener.isLastPage = false;
        this.mScrollListener.isLoading = false;
        this.mPageNumber = 0;
    }

    public void prepareLoadMore() {
        resetPresenter();
        loadMore();
        mSearchSign = SearchSign.NEW_QUERY;
    }

    public boolean onQueryTextSubmit(String queryInput) {
		if(isTooShort(queryInput)) {
            view.showNoResult("Sorry, There's No Such Batik Exist.",View.GONE);
            mSearchSign = SearchSign.NEW_QUERY;
			return false;
		}

        mQuery = queryInput;
        mFlagFilter = false;
        prepareLoadMore();

        return false;
    }

    public boolean onRetryQueryTextSubmit() {
        if(isTooShort(mQuery)) {
            view.showNoResult("Sorry, There's No Such Batik Exist.",View.GONE);
            mSearchSign = SearchSign.NEW_QUERY;
            return false;
        }

        mFlagFilter = false;
        prepareLoadMore();

        return false;
    }

    public void loadAll() {
        mQuery = "all";
        view.showProgressBar();
        prepareLoadMore();
    }

	public boolean onFilterTextSubmit(String min,String max){

        try{
            mMinPrice = Integer.parseInt(min.replace(",",""));
            mMaxPrice = Integer.parseInt(max.replace(",",""));
        } catch(Exception e){}

        if(isInvalidRange(mMinPrice,mMaxPrice)){
            view.showNoResult("Your price range is invalid.",View.VISIBLE);
            mSearchSign = SearchSign.NEW_QUERY;
            return false;
        }

        mFlagFilter = true;
        if(mQuery.equals("")) mQuery="all";
        prepareLoadMore();

        return false;
    }

    public void loadMore() {
        mSearchSign = SearchSign.LOAD_MORE;
        if(mFlagFilter) {
            mRetrofitService.setCreateCall(
                    mService.getRowsHarga(mQuery,mMinPrice,mMaxPrice,mPageNumber));
        }else {
            mRetrofitService.setCreateCall(mService.getRows(mQuery, mPageNumber));
        }
        mPageNumber++;
        mRetrofitService.doEnqueue();
    }

    private List<Batik> sortbatik(List<Batik> listBatik){
        List<Batik> judul = new ArrayList<>();
        List<Batik> other = new ArrayList<>();
        Batik batik;

        for(int i = 0; i<listBatik.size();i++){
            batik = listBatik.get(i);
            if(batik.getNama().toLowerCase().contains(mQuery.toLowerCase())){
                judul.add(batik);
            }else{
                other.add(batik);
            }
        }

        judul.addAll(other);
        return judul;
    }

    public void acceptNewList(final PageResult result) {
        if (result.getTotalElements() == 0) {
            if (mSearchSign == SearchSign.NEW_QUERY) {
                mSearchSign = SearchSign.NO_RESULT;
            } else {
                mSearchSign = SearchSign.LOAD_MORE;
            }

        } else {
            List<Batik> temp = result.getResults();
            result.setResults(sortbatik(temp));

            if (mSearchSign == SearchSign.NEW_QUERY) {
                mTotalPages = result.getTotalPages();
            } else {
                mScrollListener.isLoading = false;
            }

            if (mPageNumber > mTotalPages) {
                mScrollListener.isLastPage = true;
            }
            if(!mFlagFilter) updateFilter(result);
            mListHandler.updateRecyclerView(result);
        }

        setViewContent(mSearchSign);
    }

    @Override
    public void setViewContent(SearchSign searchSign) {
        if (searchSign == SearchSign.NEW_QUERY) {
            view.showResult();
            mSearchSign = SearchSign.LOAD_MORE;
        } else if (searchSign == SearchSign.NO_RESULT){
            if(mFlagFilter == true){
                view.showNoResult("Sorry, There's No Such Batik Exist.",View.VISIBLE);
            }else{
                view.showNoResult("Sorry, There's No Such Batik Exist.",View.GONE);
            }
            mSearchSign = SearchSign.NEW_QUERY;
        } else if (searchSign == SearchSign.CONNECTION_FAILED) {
            view.showNoResult("Failed to Connect to Server", View.GONE);
            mSearchSign = SearchSign.NEW_QUERY;
        }
    }


    private void updateFilter(PageResult result){
        SearchActivity search = view;
        search.setMaximumPrice(String.valueOf(result.getMaxPrice()));
        search.setMinimumPrice(String.valueOf(result.getMinPrice()));
    }

	boolean isTooShort(String query){
		return query.length() < 4;
	}

	boolean isInvalidRange(int min, int max){
        return min >= max;
    }
}
