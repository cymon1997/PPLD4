package ppld4.parikesit.batikita.view;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.multidex.MultiDex;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;

import ppld4.parikesit.batikita.R;
import ppld4.parikesit.batikita.adapter.BatikAdapter;
import ppld4.parikesit.batikita.model.Batik;
import ppld4.parikesit.batikita.presenter.MainPresenter;

public class MainActivity extends AppCompatActivity {
    private final int MY_PERMISSION_REQUEST_LOCATION = 1;
    private MainPresenter mPresenter;
    private Context mContext;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle("BatiKita");

        mPresenter = new MainPresenter(this);
        mContext=this;
    }

    public boolean loadPopular(List<Batik> batikList) {
        int[] listCardIds = {R.id.card_view_batik_1, R.id.card_view_batik_2,
                R.id.card_view_batik_3, R.id.card_view_batik_4,
                R.id.card_view_batik_5, R.id.card_view_batik_6};

        for (int i = 0; i < listCardIds.length; i++) {
            CardView cardBatik = (CardView) findViewById(listCardIds[i]);
            Batik batik = batikList.get(i);
            final Batik BATIK = batik;

            RelativeLayout rl = (RelativeLayout) cardBatik.getChildAt(0);
            ImageView gambarBatik = (ImageView) rl.getChildAt(0);
            TextView namaBatik = (TextView) rl.getChildAt(1);

            namaBatik.setText(batik.getNama());

            Picasso.with(this)
                    .load(batik.getGambar())
                    .fit()
                    .placeholder(R.drawable.ic_image_black)
                    .into(gambarBatik);

            cardBatik.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(mContext, InfoDetailActivity.class).putExtra("batik",BATIK);
                    mContext.startActivity(intent);
                }
            });
        }

        return true;
    }

    public boolean hidePopular() {
        /*findViewById(R.id.layout_popular).setVisibility(View.GONE);
        findViewById(R.id.layout_popular).setVisibility(View.GONE);*/
        return true;
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu (Menu menu) {
        MenuItem item = menu.findItem(R.id.searchview_keyword);
        item.setVisible(false);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.goto_page_about:
                Intent intent = new Intent(this, AboutActivity.class);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void searchByKeyword(View view) {
        startActivity(new Intent(this, SearchActivity.class));
    }

    public void findBatikNearbyMaps(View view) {
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSION_REQUEST_LOCATION);
        } else {
            startActivity(new Intent(this, MapsActivity.class));
        }
    }

    public void selectFromGallery(View view) {
        startActivity(new Intent(this, GalleryActivity.class));
    }

    public void selectFromCamera(View view) {
        startActivity(new Intent(this, CameraActivity.class));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {

        switch (requestCode) {
            case MY_PERMISSION_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startActivity(new Intent(this, MapsActivity.class));
                }
            }
        }

    }
}
