package ppld4.parikesit.batikita.view;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.squareup.picasso.Picasso;

import java.text.NumberFormat;
import java.util.Locale;

import io.fabric.sdk.android.Fabric;
import ppld4.parikesit.batikita.R;
import ppld4.parikesit.batikita.model.Batik;

public class InfoDetailActivity extends AppCompatActivity {

    ImageView imageBatik;
    TextView namaBatik;
    TextView daerahAsalBatik;
    TextView hargaBatik;
    TextView paragraphBatik;

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_info_detail);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

		Intent intent = getIntent();
        Batik batik = (Batik)intent.getSerializableExtra("batik");

        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle(batik.getNama());
        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));

        imageBatik = (ImageView) findViewById(R.id.image_batik);
        Picasso.with(this)
                .load(batik.getGambar())
                .placeholder(R.drawable.ic_image_black)
                .into(imageBatik);

        namaBatik = (TextView) findViewById(R.id.text_namaBatik);
        namaBatik.setText(batik.getNama());

        daerahAsalBatik = (TextView)findViewById(R.id.text_daerahAsalBatik);
        daerahAsalBatik.setText(batik.getDaerah_asal());

        hargaBatik = (TextView)findViewById(R.id.text_hargaBatik);

        Locale mLocale = Locale.US;
        NumberFormat mFormat = NumberFormat.getCurrencyInstance(mLocale);
        String batikMinCurrency = mFormat.format(batik.getMin_price()).replace("$","Rp. ");
        String batikMaxCurrency = mFormat.format(batik.getMax_price()).replace("$","Rp. ");
        String batikCurrency = batikMinCurrency+" - "+batikMaxCurrency;
        hargaBatik.setText(batikCurrency);

        paragraphBatik = (TextView)findViewById(R.id.text_maknaBatik);
        paragraphBatik.setText(batik.getMakna());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Enable back arrow
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        return true;
    }

    // Method for actionbar when items are selected
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onBackPressed();
        return true;
    }
}
