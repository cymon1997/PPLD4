package ppld4.parikesit.batikita.view;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.support.design.widget.AppBarLayout;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.view.View.OnClickListener;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;
import jp.wasabeef.recyclerview.animators.SlideInUpAnimator;

import ppld4.parikesit.batikita.R;
import ppld4.parikesit.batikita.adapter.BatikAdapter;
import ppld4.parikesit.batikita.presenter.SearchPresenter;
import ppld4.parikesit.batikita.utils.PriceFilterTextWatcher;
import ppld4.parikesit.batikita.utils.SearchScrollListener;

public class SearchActivity extends AppCompatActivity {

    private TextView mTextNoResult;
    private SearchPresenter mPresenter;
    private RecyclerView mRecyclerView;
    private ProgressBar mSpinner;

    private BatikAdapter mAdapter;
    private GridLayoutManager mLayoutManager;
    private SearchScrollListener mScrollListener;

    private EditText mMinimumPrice;
    private EditText mMaximumPrice;
    private Button mRetry;
    private AppBarLayout mFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_search);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle("BatiKita");
        getSupportActionBar().setSubtitle("Search");

        mTextNoResult = (TextView) findViewById(R.id.text_noResult);

        mRecyclerView = (RecyclerView)findViewById(R.id.rv_batik);
        mLayoutManager = new GridLayoutManager(this,2,GridLayoutManager.VERTICAL,false);

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setItemAnimator(new SlideInUpAnimator());

        mAdapter = new BatikAdapter(this);
        mScrollListener = new SearchScrollListener(mLayoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                mPresenter.loadMore();
            }
        };

        mRecyclerView.addOnScrollListener(mScrollListener);

        mPresenter = new SearchPresenter(this, mAdapter, mScrollListener);

        mSpinner = (ProgressBar)findViewById(R.id.progress_search);
        mSpinner.getIndeterminateDrawable().setColorFilter(Color.RED, PorterDuff.Mode.MULTIPLY);
        killProgressBar();

        mFilter = (AppBarLayout) findViewById(R.id.filter);
        mFilter.setVisibility(View.GONE);
        mMinimumPrice = (EditText) findViewById(R.id.minimum);
        mMaximumPrice = (EditText) findViewById(R.id.maximum);
        mPresenter.loadAll();
    }

    // Method for actionbar
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem item = menu.findItem(R.id.searchview_keyword);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setIconified(false);
		searchView.clearFocus();

        // auto formatting price
        mMinimumPrice.addTextChangedListener(
                new PriceFilterTextWatcher(mMinimumPrice)
        );
        mMaximumPrice.addTextChangedListener(
                new PriceFilterTextWatcher(mMaximumPrice)
        );

        // Handling text query
        searchView.setOnQueryTextListener(
                new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextChange(String newText) {
                        // Do Nothing
                        return false;
                    }

                    @Override
                    public boolean onQueryTextSubmit(String query) {
                        //Clear notification text
                        mRecyclerView.removeAllViewsInLayout();
                        mAdapter.clearList();
                        mTextNoResult.setText("");
                        mFilter.setVisibility(View.GONE);
                        showProgressBar();
                        return mPresenter.onQueryTextSubmit(query);
                    }
                }
        );

        // handling price mFiltering
        mMaximumPrice.setOnEditorActionListener(
                new TextView.OnEditorActionListener() {
                    @Override
                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                        if (actionId == EditorInfo.IME_ACTION_DONE) {
                            String mMinimumPriceText = mMinimumPrice.getText().toString();
                            String mMaximumPriceText = mMaximumPrice.getText().toString();
                            String keyword = searchView.getQuery().toString();

                            if(mMaximumPriceText.equals("")){
                                mMaximumPriceText=mMaximumPrice.getHint().toString();
                            }

                            if(mMinimumPriceText.equals("")){
                                mMinimumPriceText=mMinimumPrice.getHint().toString();
                            }

                            mRecyclerView.removeAllViewsInLayout();
                            mAdapter.clearList();
                            mTextNoResult.setText("");
                            showProgressBar();
                            setMaximumPrice(mMaximumPriceText);
                            setMinimumPrice(mMinimumPriceText);
                            return mPresenter.onFilterTextSubmit(mMinimumPriceText,mMaximumPriceText);
                        }
                        return false;
                    }
                }
        );
        return true;
    }

    // Method for actionbar, preparation before creates item
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem item = menu.findItem(R.id.goto_page_about);
        item.setVisible(false);
        return true;
    }

    // Method for actionbar when items are selected
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onBackPressed();
        return true;
    }

    public void showResult() {
       mRecyclerView.setAdapter(mAdapter);
        mFilter.setVisibility(View.VISIBLE);
        killProgressBar();
    }

    public void showNoResult(String message,int filter) {
        mFilter.setVisibility(filter);
        mAdapter.clearList();
        mRecyclerView.setAdapter(mAdapter);
        killProgressBar();
        mTextNoResult.setVisibility(View.VISIBLE);
        mTextNoResult.setText(message);
    }

    public void setMinimumPrice(String minPrice) {
        mMinimumPrice.setText("");
        mMinimumPrice.setHint(minPrice);
    }

    public void setMaximumPrice(String maxPrice) {
        mMaximumPrice.setText("");
        mMaximumPrice.setHint(maxPrice);
    }

    public void killProgressBar(){ mSpinner.setVisibility(View.GONE);}

	public void showProgressBar(){ mSpinner.setVisibility(View.VISIBLE);}
}