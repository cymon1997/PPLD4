package ppld4.parikesit.batikita.interfaces;

import ppld4.parikesit.batikita.model.PageResult;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Ferdinand on 18-Mar-17.
 */

public interface BatikService {

    @GET("{keyword}")
    Call<PageResult> getRows(@Path("keyword") String keyword,
                             @Query("page") int pageIndex);

    @GET("harga/{keyword}/{min}/{max}")
    Call<PageResult> getRowsHarga(@Path("keyword") String keyword,@Path("min") int min,
                                  @Path("max") int max, @Query("page") int pageIndex);

    @GET("popular")
    Call<PageResult> getRowsPopular();

}
