package ppld4.parikesit.batikita.interfaces;

import ppld4.parikesit.batikita.enums.SearchSign;
import ppld4.parikesit.batikita.model.PageResult;

/**
 * Created by Victor Ardianto on 5/12/2017.
 */

public interface Presenter {
    void acceptNewList(final PageResult pageResult);
    void setViewContent(SearchSign searchSign);
}
