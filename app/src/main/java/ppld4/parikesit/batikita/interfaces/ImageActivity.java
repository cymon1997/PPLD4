package ppld4.parikesit.batikita.interfaces;

import android.content.Intent;
import android.view.View;

import ppld4.parikesit.batikita.model.Batik;

/**
 * Created by Rachel on 5/17/17.
 */

public interface ImageActivity {
    public void reselectImage(View view);
    public void sendImage(View view);
    public void showNoResult(String input);
    public void openInfoDetail(Batik mBatik);
//    public void startActivity(Intent intent);
//
    void resetView();
}
