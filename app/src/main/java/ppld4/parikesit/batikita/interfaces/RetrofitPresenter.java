package ppld4.parikesit.batikita.interfaces;

import ppld4.parikesit.batikita.model.PageResult;
import ppld4.parikesit.batikita.presenter.SearchPresenter;

/**
 * Created by George A. Pitoy on 5/2/2017.
 */

public interface RetrofitPresenter {
    void acceptNewList(final PageResult result);
//    void setViewContent(SearchPresenter.SearchSign sign);
}
