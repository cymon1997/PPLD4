package ppld4.parikesit.batikita.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import ppld4.parikesit.batikita.view.InfoDetailActivity;
import ppld4.parikesit.batikita.R;
import ppld4.parikesit.batikita.model.Batik;

/**
 * Created by Ferdinand on 27-Mar-17.
 */

public class BatikAdapter extends RecyclerView.Adapter<BatikAdapter.BatikViewHolder> {

	public static class BatikViewHolder extends RecyclerView.ViewHolder {

		public CardView cardBatik;
		public ImageView gambarBatik;
		public TextView namaBatik;

		public BatikViewHolder(View itemView) {
			super(itemView);
			cardBatik = (CardView)itemView.findViewById(R.id.card_view_batik);
			gambarBatik = (ImageView)itemView.findViewById(R.id.image_batik);
			namaBatik = (TextView)itemView.findViewById(R.id.text_namaBatik);
		}
	}

    private List<Batik> batikList;
	private Context mContext;

	public BatikAdapter(Context mContext) {
		this.mContext = mContext;
        this.batikList = new ArrayList<>();
    }

	@Override
	public BatikAdapter.BatikViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        return new BatikViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.batik_row, parent, false));
	}

    @Override
    public void onBindViewHolder(BatikAdapter.BatikViewHolder viewHolder, int position) {
        Batik batik = batikList.get(position);

        // Binding nama
        TextView namaBatik = viewHolder.namaBatik;
        namaBatik.setText(batik.getNama());

        // Binding gambar and get it from URL using Picasso
        ImageView gambarBatik = viewHolder.gambarBatik;
        try{
			Picasso	.with(mContext)
					.load(batik.getGambar())
					.fit()
					.placeholder(R.drawable.ic_image_black)
					.into(gambarBatik);
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("[101]"+batik.getGambar());
		}

        // Set click function to card
        final Batik BATIK = batik;
        viewHolder.cardBatik.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                mContext.startActivity(new Intent(mContext, InfoDetailActivity.class).putExtra("batik", BATIK));
            }
        });
    }

	@Override
	public int getItemCount() {
		return batikList.size();
	}

	public void addNewList(List<Batik> newList) {
        batikList.addAll(newList);
        notifyDataSetChanged();
    }

	public void clearList(){
		batikList.clear();
	}
}
