package ppld4.parikesit.batikita.model;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Ferdinand on 18-Mar-17.
 */

@SuppressWarnings("serial")
public class Batik implements Serializable {

    @SerializedName("id")
    private int id;

    @SerializedName("nama_batik")
    private String nama;

    @SerializedName("link_batik")
    private String gambar;

    @SerializedName("makna_batik")
    private String makna;

    @SerializedName("daerah_batik")
    private String daerah_asal;

    @SerializedName("harga_rendah")
    private int min_price;

    @SerializedName("harga_tinggi")
    private int max_price;

    @SerializedName("hitung_view")
    private int view_count;

    public Batik() {
    }

    public Batik(int id, String nama, String gambar, String makna, String daerah_asal, int min_price, int max_price, int view_count) {
        this.id = id;
        this.nama = nama;
        this.gambar = gambar;
        this.makna = makna;
        this.daerah_asal = daerah_asal;
        this.min_price = min_price;
        this.max_price = max_price;
        this.view_count = view_count;
    }

    public int getId() {
        return id;
    }

    public String getNama() {
        return nama;
    }

    public String getGambar() {
        return gambar;
    }

    public String getMakna() {
        return makna;
    }

    public String getDaerah_asal() {
        return daerah_asal;
    }

    public int getMin_price() {
        return min_price;
    }

    public int getMax_price() {
        return max_price;
    }

    public int getView_count() {
        return view_count;
    }
}