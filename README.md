# PPLD4 - BatikKita
[![Coverage report](https://gitlab.com/gitlab-org/gitlab-ce/badges/master/coverage.svg?job=coverage)](https://ppl2017csui.gitlab.io/PPLD4/)

##### BatiKita merupakan sebuah aplikasi yang dapat memberikan informasi mengenai motif batik, dengan mengenali citra yang di-upload oleh user sehingga user tidak perlu melakukan pencarian informasi secara manual.

Members:
* Victor Ardianto   - 1406623751
* Ferdinand         - 1406573923
* George Albert     - 1406569781
* Geraldo           - 1406567832
* Rachel Carolina   - 1406575960

# Where to See our Build?

### untuk melihat pipeline dari build dan unit test : 
- silahkan lihat pada tab pipelines diatas

### Documentation 
- [API Documentation](http://docs.batikita.apiary.io/)